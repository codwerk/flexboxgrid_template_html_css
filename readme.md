## Flexbox Grid

Dans ce projet front-end, nous créerons un modèle de site Web HTML5 et CSS3, nous utiliserons FlexboxGrid, un système de grille léger basé sur Flexbox permettant d’aligner facilement nos éléments. Il utilise les même classes que la grille bootstrap.

### Install

`yarn add flexboxgrid --save`
